import React from 'react';
import { Provider } from 'react-redux';
import { Store } from 'redux';
import { Row } from 'react-simple-flex-grid';
import { configureStore } from './configureStore';
import './App.css';
import "react-simple-flex-grid/lib/main.css";
import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';
import 'react-toastify/dist/ReactToastify.css';
import LeftPanel from './Components/LeftPanel';
import RightPanel from './Components/RightPanel';
import { ApplicationState } from './Store';

function App() {
  const store: Store<ApplicationState> = configureStore();
  return (
    <Provider store={store}>
      <Row>
        <LeftPanel />
        <RightPanel />
      </Row>
    </Provider>
  );
}

export default App;
