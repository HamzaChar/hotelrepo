import React from 'react';
import { Amenity } from '../../Store/hotels/types';
import { 
  BarTooltip,
  BathtubTooltip,
  FitnessTooltip,
  InternetTooltip,
  NoSmockingTooltip,
  ParkingTooltip,
  PetAcceptedTooltip,
  PoolTooltip,
  RestaurantTooltip,
  TransfertTooltip,
  WifiTooltip
} from '../HotelCard/styled';

interface Props {
    amenity: Amenity;
}

function Amenities(props: Props) {

    const renderAmenities = (param: string) => {
        switch(param) {
          case 'Wi-Fi gratuit':
            return (
              <WifiTooltip>
                <i className="fas fa-wifi"></i>
              </WifiTooltip>
            );
          case 'Restaurant':
            return (
              <RestaurantTooltip>
                <i className="fas fa-utensils-alt"></i>
              </RestaurantTooltip>
            );
          case 'Non-fumeurs':
            return (
              <NoSmockingTooltip>
                <i className="fas fa-smoking-ban"></i>
              </NoSmockingTooltip>
            );
          case 'Animaux acceptés':
            return (
              <PetAcceptedTooltip>
                <i className="fas fa-dog"></i>
              </PetAcceptedTooltip>
            );
          case 'Parking disponible':
            return (
              <ParkingTooltip>
                <i className="fas fa-parking"></i>
              </ParkingTooltip>
            );
          case 'Baignoire':
            return (
              <BathtubTooltip>
                 <i className="fas fa-hot-tub"></i>
              </BathtubTooltip>
            );   
          case 'Bar':
            return (
              <BarTooltip>
                 <i className="fas fa-beer"></i>
              </BarTooltip>
            );
          case 'Piscine':
            return (
              <PoolTooltip>
                 <i className="fas fa-swimming-pool"></i>
              </PoolTooltip>
            );
          case 'Salle de Fitness':
            return (
              <FitnessTooltip>
                 <i className="fas fa-dumbbell"></i>
              </FitnessTooltip>
            ); 
          case 'Transfert aéroport': 
            return (
              <TransfertTooltip>
                <i className="fas fa-luggage-cart"></i>
              </TransfertTooltip>
            );
          case 'Accès internet':
            return (
              <InternetTooltip>
                 <i className="fab fa-internet-explorer"></i>
              </InternetTooltip>
            );
        }
    }

    return (
        <>
          {renderAmenities(props.amenity.name)}
        </>
    )
}


export default Amenities;
