import { EmailJSResponseStatus } from 'emailjs-com';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Col } from 'react-simple-flex-grid';
import { useAsync } from 'react-use';
import { Dispatch } from 'redux';
import { ToastContainer, toast } from 'react-toastify';
import { ApplicationState } from '../../Store';
import { setClientDetails } from '../../Store/shoppingCart/actions';
import { sendConfirmationEmail } from '../../Store/shoppingCart/services';
import { ClientData, ShoppingCartState } from '../../Store/shoppingCart/types';
import { FormContainer, FormTitle } from './styled';

interface Props {
    shoppingCart: ShoppingCartState,
    startDate: Date | null,
    endDate: Date | null,
    sendClientDetails: (details: ClientData) => void;
}

function ClientForm(props: Props) {
    const [dataFilled, setDataFilled] = useState(false);
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [address, setAddress] = useState('');
    const [optionalAddress, setOptionalAddress] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [zipCode, setZipCode] = useState(0);
    const [termsAndConditions, setTermsAndConditions] = useState(false);

    const handleChange = (field: string, value: string | number) => {
        switch(field) {
            case 'email': {
                setEmail(String(value));
                break;
            }
            case 'phone': {
                setPhone(String(value));
                break;
            }
            case 'address': {
                setAddress(String(value));
                break;
            }
            case 'address2': {
                setOptionalAddress(String(value));
                break;
            }
            case 'city': {
                setCity(String(value));
                break;
            }
            case 'zip': {
                setZipCode(Number(value));
                break;
            }
            case 'state': {
                setState(String(value));
                break;
            }
            case 'terms': {
                setTermsAndConditions(Boolean(value));
                break;
            }
        }
    };

    useAsync(async () => {
        if (dataFilled && props.startDate && props.endDate) {
            const result: EmailJSResponseStatus = await sendConfirmationEmail(props.shoppingCart, props.startDate, props.endDate);
            if (result.status === 200 && result.text === 'OK') {
                setDataFilled(false);
                toast("Booking reservation created succesfully!", {
                    onClose: () => window.location.reload(),
                });
            }
        }
    }, [dataFilled]);

    const submitClientData = () => {
        const data: ClientData = {
            address: address,
            optionalAddress: optionalAddress,
            email: email,
            phone: phone,
            city: city,
            state: state,
            zipCode: zipCode,
            termsAndConditions: termsAndConditions,
        };

        props.sendClientDetails(data);
        setDataFilled(true);
        return false;
    };

    return (
        <FormContainer>
            <Col span={8}>
                <FormTitle>Main Contact Information:</FormTitle>
                <form onSubmit={(e) => {
                    e.preventDefault();
                    return false;
                }}>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label htmlFor="inputEmail4">Email</label>
                            <input onChange={(e) => handleChange('email', e?.target?.value)} type="email" className="form-control" id="inputEmail4" placeholder="Email" />
                        </div>
                        <div className="form-group col-md-6">
                            <label htmlFor="inputPhone">Phone</label>
                            <input onChange={(e) => handleChange('phone', e?.target?.value)} type="phone" className="form-control" id="inputPhone" placeholder="Your phone number" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputAddress">Address</label>
                        <input onChange={(e) => handleChange('address', e?.target?.value)} type="text" className="form-control" id="inputAddress" placeholder="1234 Main St" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputAddress2">Address 2</label>
                        <input onChange={(e) => handleChange('address2', e?.target?.value)} type="text" className="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor" />
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label htmlFor="inputCity">City</label>
                            <input onChange={(e) => handleChange('city', e?.target?.value)} type="text" className="form-control" id="inputCity" />
                        </div>
                        <div className="form-group col-md-4">
                            <label htmlFor="inputState">State</label>
                            <input onChange={(e) => handleChange('state', e?.target?.value)} type="text" className="form-control" id="inputState" />
                        </div>
                        <div className="form-group col-md-2">
                            <label htmlFor="inputZip">Zip</label>
                            <input onChange={(e) => handleChange('zip', e?.target?.value)} type="text" className="form-control" id="inputZip" />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="form-check">
                            <input onChange={(e) => handleChange('terms', e?.target?.value)} className="form-check-input" type="checkbox" id="gridCheck" />
                            <label className="form-check-label" htmlFor="gridCheck">
                                I Agree to all Terms & Conditions
                            </label>
                        </div>
                    </div>
                    <button 
                        onClick={(e) => {
                            e.preventDefault();
                            submitClientData();
                            return false;
                        }}
                        type="button"
                        className="btn btn-primary"
                    >
                        Confirm my reservation
                    </button>
                </form>
            </Col>
            <ToastContainer />
        </FormContainer>
    );
}

const stateToProps = (state: ApplicationState) => ({
    shoppingCart: state?.shoppingCart,
    startDate: state?.userInputs?.startDate,
    endDate: state?.userInputs?.endDate,
});

const mappedActions = {
    sendClientDetails: (details: ClientData) => (dispatch: Dispatch) => {
        dispatch(setClientDetails(details));
    }
}

export default connect(stateToProps, mappedActions)(ClientForm);
