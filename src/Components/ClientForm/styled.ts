import styled from 'styled-components';
import { Row } from 'react-simple-flex-grid';

export const FormContainer = styled(Row)`
    padding: 5% 6%;
`;

export const FormTitle = styled.h2``;