import React from 'react';
import { Col } from 'react-simple-flex-grid';
import ImageCarousel from '../ImageCarousel';
import { HotelObject, HotelDirection } from '../../Store/hotels/types';
import Amenities from '../Amenities';
import Stars from '../Stars';
import { DirectionName, HotelContainer } from './styled';
interface Props {
  hotels?: HotelObject[];
  hotel: HotelObject;
  onHotelClicked?: () => void;
  isBooked: boolean;
}

function HotelCard(props: Props) {
  const { onHotelClicked = () => {} } = props;
  return (
    <HotelContainer>
      <Col span={12}>
        <div className="hotel-card bg-white rounded-lg shadow overflow-hidden d-block d-lg-flex">
          <div className="hotel-card_images">
            <ImageCarousel images={props.hotel.media.map(item => item.formats.medium.url)}/>
          </div>
          <div className="hotel-card_info p-4">
            <div className="d-flex align-items-center mb-2">
              <h5 className="mb-0 mr-2 text-truncate" style={{width: 250}}>{props.hotel.title}</h5>
              <a href="#!" className="ml-auto">
                <Stars star={props.hotel.stars}/>
              </a>
            </div>
            <div>
                <p className="mb-0 mr-2 text-truncate" style={{width: 350}}>{props.hotel.subtitle}</p>
            </div>
            <div className="d-flex justify-content-between align-items-end">
              <div className="hotel-card_details">
                <div className="text-muted mb-2"><i className="fas fa-map-marker-alt"></i>{props.hotel.city}</div>
                <div className="mb-2"><span className="badge badge-primary">{Number(props.hotel.rating).toFixed(1)}</span> <a href="#!" className="text-muted">{props.hotel.rating_label}</a></div>
                
                <div className="amnities d-flex mb-3">
                  {props.hotel.amenities.map(amenity => (
                      <Amenities amenity={amenity}/>
                  ))}
                </div>
                
                <ul className="hotel-checklist pl-0 mb-0">
                  {props.hotel.hotel_directions.map((direction: HotelDirection) => {
                    return (
                      <DirectionName><i className="fa fa-check text-success"></i> {direction.name}</DirectionName>
                    );
                  })}
                </ul>
              </div>
              <div className="hotel-card_pricing text-center">
                <h3>{props.hotel.price}€</h3>
                <div className="d-flex">
                  <h6 className="text-success">Taxes and fees included</h6>
                </div>
                {!props.isBooked && (
                  <button className="btn btn-primary" onClick={() => onHotelClicked()}>Book now</button>
                )}
              </div>
            </div>
          </div>
        </div>
      </Col>
    </HotelContainer>
  );
}

export default HotelCard;
