import styled from 'styled-components';
import { Row } from 'react-simple-flex-grid';

export const HotelContainer = styled(Row)`
    margin: auto;
    margin-top: 20px;
    margin-bottom: 20px;
    width: 80%;
`;

export const DirectionName = styled.li`
    font-size: 11px;
    font-family: tahoma;
`;

const Tooltip = styled.div`
    position: relative;
    text-align: center;

    &:after {
        background-color: #333;
        border-radius: 10px;
        color: #ffffff;
        display: none;
        padding: 6px 15px;
        position: absolute;
        text-align: center;
        z-index: 999;
        top: 0;
        left: 50%;
        font-size: 11px;
        text-transform: uppercase;
        transform: translate(-50%, calc(-100% - 10px));
    }

    &:hover {
        &:after {
            display: block;
        }
        :before {
            display: block;
        }
    }

    :before {
        background-color: #333;
        content: ' ';
        display: none;
        position: absolute;
        width: 15px;
        height: 15px;
        z-index: 999;
        top: 0;
        left: 50%;
        transform: translate(-50%, calc(-100% - 5px)) rotate(45deg);
    }
`;

export const WifiTooltip = styled(Tooltip)`
   &:after{
       content: 'Wifi Gratuit';
   }
`;

export const InternetTooltip = styled(Tooltip)`
   &:after{
       content: 'Accès internet';
   }
`;

export const PetAcceptedTooltip = styled(Tooltip)`
   &:after{
       content: 'Animaux acceptés';
   }
`;

export const BathtubTooltip = styled(Tooltip)`
   &:after{
       content: 'Baignoire';
   }
`;

export const BarTooltip = styled(Tooltip)`
   &:after{
       content: 'Bar';
   }
`;

export const NoSmockingTooltip = styled(Tooltip)`
   &:after{
       content: 'Non-fumeurs';
   }
`;

export const ParkingTooltip = styled(Tooltip)`
   &:after{
       content: 'Parking disponible';
   }
`;

export const PoolTooltip = styled(Tooltip)`
   &:after{
       content: 'Piscine';
   }
`;

export const RestaurantTooltip = styled(Tooltip)`
   &:after{
       content: 'Restaurant';
   }
`;

export const FitnessTooltip = styled(Tooltip)`
   &:after{
       content: 'Salle de Fitness';
   }
`;

export const TransfertTooltip = styled(Tooltip)`
   &:after{
       content: 'Transfert aéroport';
   }
`;