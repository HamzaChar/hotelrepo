import React, { useEffect, useState } from 'react';
import { BackArrow, NextArrow, Slide, Slider, SliderWrapper } from './styled';

interface Props {
    images: string[];
};

const LeftArrow = (props: { goToPrevSlide: () => void }) => {
    return (
        <BackArrow onClick={props.goToPrevSlide}>
            <i className="fas fa-chevron-left" aria-hidden="true"></i>
        </BackArrow>
    );
}


const RightArrow = (props: { goToNextSlide: () => void }) => {
    return (
        <NextArrow onClick={props.goToNextSlide}>
            <i className="fas fa-chevron-right" aria-hidden="true"></i>
        </NextArrow>
    );
}

export default function ImageSlider({ images }: Props) {

    const [currentIndex, setCurrentIndex] = useState(1);
    const [translateValue, setTranslateValue] = useState(0);
    const [slideWidth, setSlideWidth] = useState(0);

    const goToPrevSlide = () => {
        if (currentIndex === 1) {
            return;
        }
        
        setCurrentIndex(currentIndex - 1);
        setTranslateValue(translateValue + slideWidth);
    }

    const goToNextSlide = () => {
        if(currentIndex === images.length) {
            setCurrentIndex(1);
            setTranslateValue(0);
            return;
        }
        setCurrentIndex(currentIndex + 1);
        setTranslateValue((slideWidth * currentIndex)  * -1);
    }

    useEffect(() => {
        const slide = document?.querySelector('.slide') ?? null;
        if (slide) {
            setSlideWidth(Math.abs(slide?.clientWidth) ?? 0);
        }
    }, []);

    return (
        <Slider>
            <SliderWrapper
                style={{
                    transform: `translateX(${translateValue}px)`,
                    transition: 'transform ease-out 0.45s',
                }}
            >
                {images.map((image, i) => (
                    <Slide className="slide" key={i} image={image} />
                ))}
            </SliderWrapper>

            {currentIndex > 1 && <LeftArrow goToPrevSlide={goToPrevSlide} />}
            {currentIndex <= images.length && <RightArrow goToNextSlide={goToNextSlide} />}
        </Slider>
    )
}