import styled, { css } from 'styled-components';

export const Slide = styled.div<{ image: string }>`
    ${props => props.image && css`
        background-image: url(${props.image});
    `}
    
    background-size: cover;
    background-repeat: no-repeat;
    background-position: 50% 60%;
    display: inline-block;
    height: 100%;
    width: 100%;
`;

export const Slider = styled.div`
    position: relative;
    margin: 0 auto;
    overflow: hidden;
    white-space: nowrap;
    width: 400px;
    height: 100%;
`;

export const SliderWrapper = styled.div`
    position: relative;
    height: 100%;
    width: 100%;
`;
  
export const Arrow = styled.div`
    height: 27px;
    width: 27px;
    font-size: 17px;
    color: #333;
    display: flex;
    align-items: center;
    justify-content: center;
    background: rgba(255, 255, 255, .5);
    border-radius: 50%;
    cursor: pointer;
    transition: transform ease-in .1s;

    .i:before, .i:before {
        color: #222;
    }

    &:hover {
      background: rgba(255, 255, 255, .9);
    }
`;
  
export const NextArrow = styled(Arrow)`
    position: absolute;
    top: 45%;
    right: 25px;
    z-index: 999;
`;

export const BackArrow = styled(Arrow)`
    position: absolute;
    top: 45%;
    left: 25px;
    z-index: 999;
`;

