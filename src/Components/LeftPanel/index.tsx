import React, {  useState } from 'react';
import BackgroundSlider from 'react-background-slider';
import differenceInDays from 'date-fns/differenceInDays';
import { DateRange } from 'react-date-range';
import { Col } from 'react-simple-flex-grid';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import {
  GuestAreaLabel,
  GuestCountFieldArea,
  GuestEntry,
  StayingDateFields,
  StayingDatesLabel,
  StickyLeftPane
} from './styled';
import { ApplicationState } from '../../Store';
import * as UserActions from '../../Store/userInputs/actions';

type Props = {
  startDate: Date | null;
  endDate: Date | null;
  stayingLength: number;
  numberOfAdults: number;
  numberOfChildren: number;
  children?: React.ReactNode;
  dispatchDateSelection: (start: Date, end: Date, nights: number) => void;
  dispatchAdultsCount: (adults: number) => void;
  dispatchChildrenCount: (children: number) => void;
};

function LeftPanel({ children, ...props}: Props) {
    const [state, setState] = useState([
      {
        startDate: new Date(),
        endDate: new Date(),
        key: 'selection'
      }
    ]);
    const [numberOfAdults, setNumberOfAdults] = useState(0);
    const [numberOfChildren, setNumberOfChildren] = useState(0);

    return (
      <StickyLeftPane span={4}>
        <BackgroundSlider
          images={[
              'https://res.cloudinary.com/deemy0vbo/image/upload/v1625898508/paris_3_xqx2o4.jpg',
              'https://res.cloudinary.com/deemy0vbo/image/upload/v1625898507/paris_4_ez4zqb.jpg',
              'https://res.cloudinary.com/deemy0vbo/image/upload/v1625898507/paris_2_z5oo91.jpg',
              'https://res.cloudinary.com/deemy0vbo/image/upload/v1625898508/paris_1_psueei.jpg',
          ]}
          duration={10} transition={2}
        />
        <StayingDateFields>
          <StayingDatesLabel>When are you coming?</StayingDatesLabel>
          <DateRange
            editableDateInputs={true}
            onChange={item => {
              setState([item.selection]);
              const nights = differenceInDays(item.selection?.endDate, item.selection?.startDate);
              props.dispatchDateSelection(item.selection?.startDate, item.selection?.endDate, nights);
            }}
            moveRangeOnFirstSelection={false}
            ranges={state}
          />
        </StayingDateFields>
        <GuestCountFieldArea>
          <GuestAreaLabel>Who's coming?</GuestAreaLabel>
          <GuestEntry>
            <Col span={3}>
              <p>Adults:</p>
            </Col>
            <Col span={9}>
              <input
                className="form-control"
                type="number"
                max={5}
                min={1}
                value={numberOfAdults}
                onChange={(e) => {
                  setNumberOfAdults(Number(e?.target?.value ?? 0));
                  props.dispatchAdultsCount(Number(e?.target?.value ?? 0));
                }}
              />
            </Col>
          </GuestEntry>
          <GuestEntry>
            <Col span={3}>
              <p>Children:</p>
            </Col>
            <Col span={9}>
              <input
                className="form-control"
                type="number"
                max={3}
                min={1}
                value={numberOfChildren}
                onChange={(e) => {
                  setNumberOfChildren(Number(e?.target?.value ?? 0));
                  props.dispatchChildrenCount(Number(e?.target?.value ?? 0));
                }}
              />
            </Col>
          </GuestEntry>
        </GuestCountFieldArea>
        <div>
          { children }
        </div>
      </StickyLeftPane>
    );
}

const mapStateToProps = (state: ApplicationState) => ({
  startDate: state.userInputs.startDate,
  endDate: state.userInputs.endDate,
  stayingLength: state.userInputs.stayingLength,
  numberOfAdults: state.userInputs.numberOfAdults,
  numberOfChildren: state.userInputs.numberOfChildren,
});

const mapDispatchToProps = {
  dispatchDateSelection: (start: Date, end: Date, nights: number) => (dispatch: Dispatch) => {
    dispatch(UserActions.setDateRange(start, end, nights));
  },
  dispatchAdultsCount: (adults: number) => (dispatch: Dispatch) => {
    dispatch(UserActions.setAdultsCount(adults));
  },
  dispatchChildrenCount: (children: number) => (dispatch: Dispatch) => {
    dispatch(UserActions.setChildrenCount(children));
  },
};

export default connect(mapStateToProps, mapDispatchToProps)(LeftPanel);