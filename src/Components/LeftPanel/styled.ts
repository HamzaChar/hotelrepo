import styled from 'styled-components';
import { Row, Col } from 'react-simple-flex-grid';

export const StickyLeftPane = styled(Col)`
  position: fixed;
  top: 0;
  bottom: 0;
`;

export const StayingDatesLabel = styled.div`
  color: white;
  font-weight: bold;
  font-size: 18px;
  background-color: #007bff;
  padding: 5px;
  margin-bottom: 5px;
`;

export const StayingDateFields = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10% 0%;
  align-items: start;
  justify-content: center;
  width: 70%;
  margin: auto;
  overflow: hidden;
`;

export const GuestCountFieldArea = styled(StayingDateFields)`
  padding: 0% 0%;
  background-color: rgba(255, 255, 255, .9);
  width: 70%;
  margin: auto;
`;

export const GuestAreaLabel = styled(StayingDatesLabel)`
  background-color: #007bff;
  color: white;
`;

export const GuestEntry = styled(Row)`
  width: 100%;
  color: black;
  padding: 8px;
`;