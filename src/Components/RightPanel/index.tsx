import React, { ReactNode } from 'react'
import { connect } from 'react-redux';
import Lottie from 'react-lottie';
import HotelCard from '../HotelCard'
import { Content, StickyRightPane } from './styled'
import { fetchHotels } from '../../Store/hotels/services';
import { HotelObject } from '../../Store/hotels/types';
import { useAsync } from 'react-use';
import { ApplicationState } from '../../Store';
import { Dispatch } from 'redux';
import { setSelectedHotel } from '../../Store/shoppingCart/actions';
import animatedLoaderData from '../../loading-data.json';
import ShoppingCart from '../ShoppingCart';

interface RightPanelProps {
    isLoading: boolean;
    hotels?: HotelObject[];
    fetchHotels: () => any;
    dispatchSelectedHotel: (hotel: HotelObject, nights?: number) => any;
    children?: ReactNode;
    startDate?: Date | null;
    endDate: Date | null;
    stayingLength: number;
    numberOfAdults: number;
    numberOfChildren: number;
    selectedHotelId: number;
}

function RightPanel(props: RightPanelProps) {
    useAsync(async () => {
        props.fetchHotels();
    }, []);

    return (
        <StickyRightPane span={8}>
            <Content>
                {props.isLoading && (
                    <Lottie
                        options={{
                            loop: true,
                            autoplay: true, 
                            animationData: animatedLoaderData,
                            rendererSettings: {
                                preserveAspectRatio: 'xMidYMid slice'
                            }
                        }}
                        height={400}
                        width={400}
                    />
                )}
                <ShoppingCart />
                {!props.isLoading &&
                    props.hotels
                        ?.filter((h: HotelObject) => h.title !== null && h.price !== null)
                        ?.map((hotel: HotelObject) => (
                            <HotelCard
                                onHotelClicked={() => props.dispatchSelectedHotel(hotel, props?.stayingLength ?? 1)}
                                key={hotel.id} 
                                hotel={hotel}
                                isBooked={hotel.id === props.selectedHotelId}
                            />
                        ))
                }
            </Content>
        </StickyRightPane>
    )
}

const mapStateToProps = (state: ApplicationState) => {
	return {
		isLoading: state.hotels.loading,
        hotels: state.hotels.data,
        startDate: state.userInputs.startDate,
        endDate: state.userInputs.endDate,
        stayingLength: state.userInputs.stayingLength,
        numberOfAdults: state.userInputs.numberOfAdults,
        numberOfChildren: state.userInputs.numberOfChildren,
        selectedHotelId: state?.shoppingCart?.selectedHotel?.id ?? 0,
	}
};

const mappedActions = {
    fetchHotels,
    dispatchSelectedHotel: (hotel: HotelObject, nights?: number) => (dispatch: Dispatch) => {
        dispatch(setSelectedHotel(hotel, nights));
    },
};

export default connect(mapStateToProps, mappedActions)(RightPanel);