import styled from 'styled-components';
import { Col } from 'react-simple-flex-grid';

export const StickyRightPane = styled(Col)`
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  background-image: url('https://res.cloudinary.com/deemy0vbo/image/upload/v1625898507/circle-background-pattern_opgssf.png');
  overflow-y: auto;
  overflow-x: hidden;
  padding-top: 100px;
  padding-bottom: 100px;
`;

export const Content = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;