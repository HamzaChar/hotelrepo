import React, { useState } from 'react';
import { connect } from 'react-redux';
import ReactModal from 'react-modal';
import { Row, Col } from 'react-simple-flex-grid';
import HotelCard from '../HotelCard';
import {ShopingCartTrigger} from './styled'
import { HotelObject } from '../../Store/hotels/types';
import { ApplicationState } from '../../Store';
import { format } from 'date-fns';
import ClientForm from '../ClientForm';

interface ShoppingCartProps {
    selectedHotel: HotelObject | null;
    totalPrice: number;
    startDate: Date | null;
    endDate: Date | null;
    stayingLength: number;
    numberOfAdults: number;
    numberOfChildren: number;
}

function ShoppingCart(props: ShoppingCartProps) {
    const [modalOpened, setModalOpened] = useState(false);
    return (
        <>
            <ShopingCartTrigger onClick={() => setModalOpened(true)}>
            <i className="fas fa-shopping-basket"></i>
            </ShopingCartTrigger>
            <ReactModal isOpen={modalOpened}>
                <button onClick={() => setModalOpened(false)}>Close</button>
                {props.selectedHotel && (
                    <>
                        <Row>
                            <Col span={8}>
                                <HotelCard 
                                    key={props.selectedHotel?.id} 
                                    hotel={props.selectedHotel}
                                    isBooked
                                />
                            </Col>
                            <Col span={4}>
                                <p><strong>Total price: </strong>{props.totalPrice}€</p>
                                <p><strong>For: </strong>{props.numberOfAdults} Adult(s), and {props.numberOfChildren} Children</p>
                                {props.startDate && (
                                    <p><strong>Starting from: </strong>{format(props?.startDate, 'd MMMM Y')}</p>
                                )}
                                {props.endDate && (
                                    <p><strong>To: </strong>{format(props?.endDate, 'd MMMM Y')}</p>
                                )}
                                <p><strong>Staying for: </strong>{props.stayingLength - 1} Days, and {props.stayingLength} Nights</p>
                            </Col>
                        </Row>
                        <ClientForm />
                    </>
                )}
            </ReactModal>
        </>
    );
}

const mapStateToProps = (state: ApplicationState) => {
	return {
        selectedHotel: state?.shoppingCart?.selectedHotel,
        totalPrice: state?.shoppingCart?.totalPrice,
        startDate: state.userInputs.startDate,
        endDate: state.userInputs.endDate,
        stayingLength: state.userInputs.stayingLength,
        numberOfAdults: state.userInputs.numberOfAdults,
        numberOfChildren: state.userInputs.numberOfChildren,
	}
};

export default connect(mapStateToProps)(ShoppingCart);