import styled from 'styled-components';

export const ShopingCartTrigger = styled.button`
  position: absolute;
  top: 10px;
  right: 10px;
  background-color: #007bff;
  width: 60px;
  height: 60px;
  border: none;
  border-radius: 100%;
  color: white;
  font-size: 20px;
  padding: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  outline: none;
  user-select: none;
  &:hover, &:active {
    outline: none;
    user-select: none;
  }
`;