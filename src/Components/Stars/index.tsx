import React from 'react';

interface Props {
    star: number;
}

function Stars(props: Props) {

    const renderStars = (star: number) => {
        let stars: any = [];
        for(let i = 0; i < star; i++) {
          stars.push(<i key={i} className="fa fa-star text-warning"></i>); 
        }
        return stars;
    }

    return (
        <>
          {renderStars(props.star)}
        </>
    )
}


export default Stars;
