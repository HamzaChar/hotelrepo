import { HotelActionTypes, HotelObject } from './types';

export const fetchHotels = (loading: boolean) => ({ type: HotelActionTypes.FETCH_ALL_HOTELS, payload: {loading} });

export const fetchHotelsSucces = (data: HotelObject[], loading: boolean) => ({ type: HotelActionTypes.FETCH_HOTELS_SUCESS, payload: {data, loading} });

export const fetchHotelsFailed = (error: string, loading: boolean) => ({ type: HotelActionTypes.FETCH_HOTELS_FAILED, payload: { error, loading }});

export const setHotelId = (id: number) => ({
    type: HotelActionTypes.SET_HOTEL_ID, 
    payload: {
        hotelId: id,
    }
});