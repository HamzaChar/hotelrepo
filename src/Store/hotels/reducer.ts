import { AnyAction } from "redux";
import { HotelState, HotelActionTypes } from "./types";

const initialState: HotelState = {
    loading: false,
    data: [],
    error: '',
    hotelId: 0
};

function reducer(state: HotelState = initialState, action: AnyAction) {
    const { type = '', payload = {} } = action;
    switch (type) {
      case HotelActionTypes.SET_HOTEL_ID:
      case HotelActionTypes.FETCH_ALL_HOTELS: 
      case HotelActionTypes.FETCH_HOTELS_SUCESS:
      case HotelActionTypes.FETCH_HOTELS_FAILED: {
        return {
          ...state,
          ...payload,
        };
      }
      default: {
          return state;
      }
    }
}

export {reducer as HotelsReducer};