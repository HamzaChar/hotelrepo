import { Dispatch } from "redux";
import * as HotelActions from './actions';
import { HotelObject } from "./types";

export const fetchHotels = () => async (dispatch: Dispatch) => {
    dispatch(HotelActions.fetchHotels(true));
    try {
        const hotelsResp = await fetch('https://mighty-brook-95866.herokuapp.com/hotels');
        const hotels: HotelObject[] = await hotelsResp.json();
        dispatch(HotelActions.fetchHotelsSucces(hotels, false));
    } catch (e) {
        dispatch(HotelActions.fetchHotelsFailed(e?.getMessage() ?? 'Loading Failed', false));
    }
};