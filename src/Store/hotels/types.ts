
export interface ProviderMetadata {
    public_id: string;
    resource_type: string;
}

export interface Small {
    ext: string;
    url: string;
    hash: string;
    mime: string;
    name: string;
    path?: any;
    size: number;
    width: number;
    height: number;
    provider_metadata: ProviderMetadata;
}

export interface ProviderMetadata2 {
    public_id: string;
    resource_type: string;
}

export interface Medium2 {
    ext: string;
    url: string;
    hash: string;
    mime: string;
    name: string;
    path?: any;
    size: number;
    width: number;
    height: number;
    provider_metadata: ProviderMetadata2;
}

export interface ProviderMetadata3 {
    public_id: string;
    resource_type: string;
}

export interface Thumbnail {
    ext: string;
    url: string;
    hash: string;
    mime: string;
    name: string;
    path?: any;
    size: number;
    width: number;
    height: number;
    provider_metadata: ProviderMetadata3;
}

export interface Formats {
    small: Small;
    medium: Medium2;
    thumbnail: Thumbnail;
}

export interface ProviderMetadata4 {
    public_id: string;
    resource_type: string;
}

export interface HotelMedia {
    id: number;
    name: string;
    alternativeText: string;
    caption: string;
    width: number;
    height: number;
    formats: Formats;
    hash: string;
    ext: string;
    mime: string;
    size: number;
    url: string;
    previewUrl?: any;
    provider: string;
    provider_metadata: ProviderMetadata4;
    created_at: Date;
    updated_at: Date;
}

export interface Amenity {
    id: number;
    name: string;
    published_at: Date;
    created_at: Date;
    updated_at: Date;
}

export interface HotelDirection {
    id: number;
    name: string;
    hotel: number;
    published_at: Date;
    created_at: Date;
    updated_at: Date;
}

export interface HotelObject {
    id: number;
    title: string;
    subtitle: string;
    price: number;
    city: string;
    rating: number;
    rating_label: string;
    taxes_and_fees: string;
    published_at: Date;
    created_at: Date;
    updated_at: Date;
    stars: number;
    media: HotelMedia[];
    amenities: Amenity[];
    hotel_directions: HotelDirection[];
}

export interface HotelState {
    data: HotelObject[];
    loading: boolean;
    error: string;
    hotelId: number;
}

export enum HotelActionTypes {
    FETCH_ALL_HOTELS = '@app/hotels/FETCH_ALL_HOTELS',
    FETCH_HOTELS_SUCESS  = '@app/hotels/FETCH_HOTELS_SUCESS',
    FETCH_HOTELS_FAILED  = '@app/hotels/FETCH_HOTELS_FAILED',
    SET_HOTEL_ID  = '@app/hotels/SET_HOTEL_ID',
}