import { combineReducers } from "redux";
import { HotelsReducer } from "./hotels/reducer";
import { HotelState } from "./hotels/types";
import { ShoppingCartReducer } from "./shoppingCart/reducer";
import { ShoppingCartState } from "./shoppingCart/types";
import { UserInputReducer } from "./userInputs/reducer";
import { UserInputState } from "./userInputs/types";

export interface ApplicationState {
    hotels: HotelState;
    userInputs: UserInputState;
    shoppingCart: ShoppingCartState;
}

export const getRootReducer = () => combineReducers({
    hotels: HotelsReducer,
    userInputs: UserInputReducer,
    shoppingCart: ShoppingCartReducer,
});