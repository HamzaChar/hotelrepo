import { HotelObject } from '../hotels/types';
import { CartActionTypes, ClientData } from './types';

export const setSelectedHotel = (hotel: HotelObject, nights: number = 1) => ({
    type: CartActionTypes.SET_SELECTED_HOTEL, 
    payload: {
        selectedHotel: hotel,
        totalPrice: hotel.price * nights,
    }
});

export const setClientDetails = (details: ClientData) => ({
    type: CartActionTypes.SET_CLIENT_DATA, 
    payload: {
        clientDetails: { ...details },
    }
});