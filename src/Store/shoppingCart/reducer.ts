import { AnyAction } from "redux";
import { ShoppingCartState, CartActionTypes } from "./types";

const initialState: ShoppingCartState = {
    selectedHotel: null,
    totalPrice: 0,
    clientDetails: null,
};

function reducer(state: ShoppingCartState = initialState, action: AnyAction) {
    const { type = '', payload = {} } = action;
    switch (type) {
      case CartActionTypes.SET_CLIENT_DATA:
      case CartActionTypes.SET_SELECTED_HOTEL: {
        return {
          ...state,
          ...payload,
        };
      }
      default: {
          return state;
      }
    }
}

export {reducer as ShoppingCartReducer};