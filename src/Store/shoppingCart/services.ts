import { format } from 'date-fns';
import emailjs from 'emailjs-com';
import { ShoppingCartState } from './types';

export const sendConfirmationEmail = async (shoppingCart: ShoppingCartState, startDate: Date, endDate: Date) => {
    const message = {
        to_name: shoppingCart.clientDetails?.email,
        message: `
            <p><strong>Hotel name: </strong>${shoppingCart.selectedHotel?.title}</p>
            <p><strong>Total Price: </strong>${shoppingCart.totalPrice}€</p>
            <p><strong>Starting from: </strong>${format(startDate, 'd MMMM Y')}</p>
            <p><strong>To: </strong>${format(endDate, 'd MMMM Y')}</p>
        `,
    };

    const result = await emailjs.send('service_5is8gh3', 'template_hxy3s6a', message, 'user_6BsMVgkzDDCtoDV6FMQbR');
    return result;
};