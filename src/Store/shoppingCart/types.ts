import { HotelObject } from "../hotels/types";

export interface ClientData {
    email: string;
    phone: string;
    address: string;
    optionalAddress: string;
    city: string;
    state: string;
    zipCode: number;
    termsAndConditions: boolean;
}

export interface ShoppingCartState {
    selectedHotel: HotelObject | null;
    totalPrice: number;
    clientDetails: ClientData | null;
}

export enum CartActionTypes {
    SET_SELECTED_HOTEL = '@app/shopping-cart/SET_SELECTED_HOTEL',
    SET_CLIENT_DATA = '@app/shopping-cart/SET_CLIENT_DATA',
}