import { UserInputActionTypes } from './types';

export const setDateRange = (start: Date, end: Date, nights: number) => ({
    type: UserInputActionTypes.SET_DATES_RANGE, 
    payload: {
        startDate: start,
        endDate: end,
        stayingLength: nights,
    }
});

export const setAdultsCount = (adults: number) => ({
    type: UserInputActionTypes.SET_ADULTS_COUNT, 
    payload: {
        numberOfAdults: adults,
    }
});

export const setChildrenCount = (children: number) => ({
    type: UserInputActionTypes.SET_CHILDREN_COUNT, 
    payload: {
        numberOfChildren: children,
    }
});