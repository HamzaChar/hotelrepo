import { AnyAction } from "redux";
import { UserInputState, UserInputActionTypes } from "./types";

const initialState: UserInputState = {
    startDate: null,
    endDate: null,
    numberOfAdults: 0,
    numberOfChildren: 0,
    stayingLength: 0,
};

function reducer(state: UserInputState = initialState, action: AnyAction) {
    const { type = '', payload = {} } = action;
    switch (type) {
      case UserInputActionTypes.SET_ADULTS_COUNT: 
      case UserInputActionTypes.SET_CHILDREN_COUNT:
      case UserInputActionTypes.SET_DATES_RANGE: {
        return {
          ...state,
          ...payload,
        };
      }
      default: {
          return state;
      }
    }
}

export {reducer as UserInputReducer};