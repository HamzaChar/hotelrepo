export interface UserInputState {
    startDate: Date | null;
    endDate: Date | null;
    stayingLength: number;
    numberOfAdults: number;
    numberOfChildren: number;
}

export enum UserInputActionTypes {
    SET_DATES_RANGE = '@app/user-inputs/SET_DATES_RANGE',
    SET_ADULTS_COUNT  = '@app/user-inputs/SET_ADULTS_COUNT',
    SET_CHILDREN_COUNT  = '@app/user-inputs/SET_CHILDREN_COUNT',
}