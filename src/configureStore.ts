import { Store, createStore, applyMiddleware, compose } from "redux";
import thunk from 'redux-thunk';
import { ApplicationState, getRootReducer } from "./Store";

const composeEnhancer = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;

export const configureStore = (): Store<ApplicationState> => {
    let initialState: ApplicationState = {
        hotels: {
            data: [],
            loading: false,
            error: '',
            hotelId: 0
        },
        userInputs: {
            startDate: null,
            endDate: null,
            stayingLength: 0,
            numberOfAdults: 0,
            numberOfChildren: 0,
        },
        shoppingCart: {
            selectedHotel: null,
            totalPrice: 0,
            clientDetails: null,
        }
    };

    const middlewares = [thunk];

    const store =  createStore(
        getRootReducer(),
        initialState,
        composeEnhancer(applyMiddleware(...middlewares))
    );

    return store;
}